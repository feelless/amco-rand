package main

import (
	"bytes"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"os"
	"time"

	jsoniter "github.com/json-iterator/go"
)

type MessageForm struct {
	Message  map[string]interface{} `json:"message"`
	ClientID string                 `json:"clientID"`
}
type produceObject struct {
	Await   bool              `json:"aWait"`
	Message MessageForm       `json:"message"`
	Topics  map[string]string `json:"topics"`
}
type ResponseTringger struct {
	Result     MessageForm `json:"result"`
	Error      string      `json:"error"`
	StatusCode int         `json:"statusCode"`
}

func main() {
	var topicReq string
	var topicRes string
	var groupID string
	var baseUrl string

	if len(os.Args) == 1 {
		topicReq = "rand-op-req"
		topicRes = "rand-op-res"
		groupID = "rand-op"
		baseUrl = "http://localhost:7890"
	} else {

		args := os.Args[1:]
		baseUrl = args[0]
		topicReq = args[1]
		topicRes = args[2]
		groupID = args[3]
	}

	log.Println(baseUrl, topicReq, topicRes, groupID)

	urlConsume := baseUrl + "/api/consumer"
	urlProduce := baseUrl + "/api/producer"
	body := make(map[string]interface{})
	body["topics"] = []string{topicReq}
	body["groupID"] = groupID
	bodyBytes, err := jsoniter.Marshal(body)
	if err != nil {
		log.Println(err)
	}

	for true {
		var rt *ResponseTringger
		resp, err := http.Post(urlConsume, "application/json", bytes.NewBuffer(bodyBytes))
		if err != nil {
			log.Println(err)
		}
		defer resp.Body.Close()
		rawRespBody, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			log.Println(err)
		}
		if err = jsoniter.Unmarshal(rawRespBody, &rt); err != nil {
			log.Println(err)
		}

		if rt.Result.Message["rand"] == "getOperator" {

			var po produceObject
			numberMap := make(map[string]interface{})
			topicsMap := make(map[string]string)
			operators := []string{"+", "-", "*", "/", "%"}
			rand.Seed(time.Now().UnixNano())
			numrand := rand.Intn(5)
			println(operators[numrand])

			topicsMap["sending"] = topicRes
			numberMap["rand"] = operators[numrand]
			po.Await = false
			po.Message.Message = numberMap
			po.Message.ClientID = rt.Result.ClientID
			po.Topics = topicsMap

			log.Println(po)

			bytesBody, err := jsoniter.Marshal(&po)
			if err != nil {
				log.Println(err)
			}
			resp, err := http.Post(urlProduce, "application/json", bytes.NewBuffer(bytesBody))
			if err != nil {
				log.Println(err)
			}
			defer resp.Body.Close()
			data, err := ioutil.ReadAll(resp.Body)
			if err != nil {
				log.Println(err)
			}
			var rt *ResponseTringger
			if err := jsoniter.Unmarshal(data, &rt); err != nil {
				log.Println(err)
			}
			log.Println(rt)

		}

	}

}
